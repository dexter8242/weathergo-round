package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/charmbracelet/log"
	"io"
	"net/http"
	"net/url"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

type Location struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

type WeatherResponse struct {
	Weather []Weather `json:"weather"`
}

type Weather struct {
	Forecast    string `json:"main"`
	Description string `json:"description"`
}

func main() {
	fmt.Println("Enter OpenWeatherMap API Key >>>: ")
	scanner.Scan()
	key := scanner.Text()

	fmt.Println("Enter city >>>: ")
	scanner.Scan()
	city := scanner.Text()

	// Convert the supplied city into actual coordinates
	lon, lat := convertToCoordinates(city, key)

	// Use fmt.Sprintf in order to return the string of lat & lon grabbed from city
	forecast, desc := getWeather(fmt.Sprintf("%f", lat), fmt.Sprintf("%f", lon), key)

	// Print the current forecast
	fmt.Printf("The current forecast is: %v\nWith a detailed description of: %v.\n\n", forecast, desc)
}

func concatenateURL(city string, key string) string {
	// Grab the API key and the city to lookup weather for
	headers := url.Values{
		"q":     []string{city},
		"appid": []string{key},
	}

	// Concatenate the URL with the parameters provided
	url := "http://api.openweathermap.org/geo/1.0/direct?" + headers.Encode()

	return url
}

// Function that takes a city and an OpenWeatherMap API key in order to return
// the coordinates for the specified city.
func convertToCoordinates(city string, key string) (lon float64, lat float64) {
	// Grab the API key and the city to lookup weather for
	headers := url.Values{
		"q":     []string{city},
		"appid": []string{key},
	}

	// Concatenate the URL with the parameters provided
	url := "http://api.openweathermap.org/geo/1.0/direct?" + headers.Encode()

	// Send an API call to grab coordinates
	resp, err := http.Get(url)

	// Handle errors
	if err != nil {
		log.Error("Failed to query API", "err", err)
	}

	// Store the actual body received as jsonData
	jsonData, err := io.ReadAll(resp.Body)

	// Create a slice to hold the struct of coordinates
	var coordinates []Location

	// Unmarshal the JSON data
	err = json.Unmarshal([]byte(jsonData), &coordinates)

	// Handle errors
	if err != nil {
		log.Error("Failed to unmarshal received data", "err", err)
	}

	// Return the longitude and latitude of the city provided
	return coordinates[0].Lon, coordinates[0].Lat
}

// Takes coordinates in the form of latitude and longitude, as well as an OpenWeatherMap
// API key and returns the current weather forecast. If an error occurs, it returns two
// empty strings.
func getWeather(lat, lon string, key string) (forecast string, description string) {
	// Grab the API key and the city to lookup weather for
	headers := url.Values{
		"lat":   []string{lat},
		"lon":   []string{lon},
		"appid": []string{key},
	}

	// Concatenate the URL with the parameters provided
	url := "http://api.openweathermap.org/data/2.5/weather?" + headers.Encode()

	// Send an API call to grab coordinates
	resp, err := http.Get(url)

	// Handle errors
	if err != nil {
		log.Error("Failed to query API", "err", err)
	}

	// Store the actual body received as jsonData
	jsonData, err := io.ReadAll(resp.Body)

	var data WeatherResponse

	err = json.Unmarshal([]byte(jsonData), &data)
	fmt.Println(data)

	if err != nil {
		log.Error("Failed to unmarshal response", "err", err)
	}

	for _, weather := range data.Weather {
		return weather.Forecast, weather.Description
	}

	// An error occured
	return "", ""
}
