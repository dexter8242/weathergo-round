build:
	go build -o ./bin/go-round

run: build
	go run .
