# WeatherGo-Round

WeatherGo-Round is a minimalist weather app written in Go. It provides users with the current weather forecast and has exciting plans for future enhancements. 

## Features

- **Current Weather Forecast**: Get a description for the current forecast for your location.

## Planned Features

- [d] **Time-Specific Forecast**: Get the weather forecast for a specific time period. Whether it's planning your morning jog or a picnic in the park, WeatherGo-Round will keep you informed. (**Deffered** - Currently broken but implemented in testing branch)

- [ ] **Date-Specific Forecast**: Plan your activities in advance by accessing the weather forecast for a specific date. No more surprises or last-minute changes of plans.

## Installation

### Build from Source (Requires Go)

To install WeatherGo-Round from source, follow these simple steps:

1. Clone the repository: `git clone https://gitlab.com/dexter8242/WeatherGo-Round.git`
2. Navigate to the project directory: `cd WeatherGo-Round`
3. Build the application: `make`
4. Enjoy the ride: `make run`

## Usage

Once WeatherGo-Round is up and running, you can access the current weather forecast by providing it your OpenWeatherMap API key and the city to get the forecast for. Additional commands will be added in the future to access time-specific and date-specific forecasts.

## Contributing

WeatherGo-Round welcomes contributions from developers of all skill levels. If you have an idea for a new feature or want to improve the existing codebase, feel free to submit a pull request. 

Before contributing, please review the [contribution guidelines](https://gitlab.com/dexter8242/WeatherGo-Round/blob/master/CONTRIBUTING.md).

## License

WeatherGo-Round is released under the [MIT License](https://gitlab.com/dexter8242/WeatherGo-Round/blob/master/LICENSE). Feel free to use, modify, and distribute the code as per the terms of the license.

## Contact

If you have any questions, suggestions, or feedback, please reach out to me at contact@dexterslabs.simplelogin.com. I would love to hear from you!

Enjoy the ride with WeatherGo-Round!