# Contributing to WeatherGo-Round

Thank you for considering contributing to WeatherGo-Round! We appreciate your interest in improving our weather app. This document outlines the guidelines for contributing to the project. Please take a moment to read through this guide before submitting your contributions.

## Table of Contents

- [Code of Conduct](#code-of-conduct)
- [How Can I Contribute?](#how-can-i-contribute)
- [Reporting Bugs](#reporting-bugs)
- [Suggesting Enhancements](#suggesting-enhancements)
- [Working on Issues](#working-on-issues)
- [Submitting Pull Requests](#submitting-pull-requests)
- [License](#license)

## Code of Conduct

We have adopted a Code of Conduct that we expect all contributors to adhere to. By participating in this project, you are expected to uphold this code. Please read the [Code of Conduct](https://gitlab.com/dexter8242/WeatherGo-Round/blob/master/CODE_OF_CONDUCT.md) before proceeding.

## How Can I Contribute?

You can contribute to WeatherGo-Round in various ways:

- Reporting bugs
- Suggesting enhancements
- Working on existing issues
- Submitting pull requests

Let's explore each of these in detail.

## Reporting Bugs

If you encounter a bug while using WeatherGo-Round, we want to know about it! Please take the following steps to report the bug:

1. Ensure the bug hasn't already been reported by searching the [issue tracker](https://gitlab.com/dexter8242/WeatherGo-Round/issues).
2. If the bug hasn't been reported, [open a new issue](https://gitlab.com/dexter8242/WeatherGo-Round/issues/new).
3. Provide a clear and descriptive title for the issue.
4. Describe the steps to reproduce the bug.
5. Include any relevant error messages or screenshots.

## Suggesting Enhancements

We welcome suggestions for new features or improvements to existing functionality. To suggest an enhancement:

1. Check the [issue tracker](https://gitlab.com/dexter8242/WeatherGo-Round/issues) to ensure your suggestion hasn't already been made.
2. If it's a new suggestion, [open a new issue](https://gitlab.com/dexter8242/WeatherGo-Round/issues/new).
3. Clearly describe the enhancement you would like to see, providing any necessary details or examples.
4. Explain why you believe this enhancement would be beneficial to WeatherGo-Round.

## Working on Issues

If you find an open issue that you would like to work on, please leave a comment on the issue to let others know you're working on it. This will help avoid duplicating efforts.

## Submitting Pull Requests

When submitting a pull request, please follow these guidelines:

1. Fork the repository and create a new branch for your feature or bug fix.
2. Include any necessary tests for your changes.
3. Provide a clear and descriptive pull request title.
4. Describe the changes you've made in the pull request description.
5. Reference any related issues using the `#issue_number` syntax.

## License

By contributing to WeatherGo-Round, you agree that your contributions will be licensed under the [MIT License](https://gitlab.com/dexter8242/WeatherGo-Round/blob/master/LICENSE).

## Contact

If you have any further questions or need assistance, please feel free to reach out to me at contact@dexterslabs.simplelogin.com.

Thank you for your interest in contributing to WeatherGo-Round! We appreciate your support in making our weather app even better.
